//
//  ViewController.swift
//  PandawaUpdatesPrototype
//
//  Created by iLoops on 2/28/16.
//  Copyright © 2016 ptdmc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBAction func button1Pressed(sender:AnyObject){
        print("button1 pressed")
        if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("view2") as? UIViewController {
            presentViewController(resultController, animated: true, completion: nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

