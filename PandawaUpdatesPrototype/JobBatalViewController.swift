//
//  JobBatalViewController.swift
//  PandawaUpdatesPrototype
//
//  Created by iLoops on 3/1/16.
//  Copyright © 2016 ptdmc. All rights reserved.
//

import UIKit

class JobBatalViewController: UIViewController, UIPickerViewDelegate {
    
    @IBAction func showAlert() {
        let alertController = UIAlertController(title: "Konfirm Batal", message: "Yakin ingin melakukan pembatalan?", preferredStyle: .Alert)
    
        let defaultAction = UIAlertAction(title: "Ya", style: .Default){action -> Void in
            if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("joblist") as? UIViewController {
                self.presentViewController(resultController, animated: true, completion: nil)
            }
        }
        let noAction = UIAlertAction(title:"No", style: .Destructive, handler: nil)
        alertController.addAction(defaultAction)
        alertController.addAction(noAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func buttonKembaliPressed(sender:AnyObject){
        print("button kembali pressed")
        if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("jobdetail") as? UIViewController {
            presentViewController(resultController, animated: true, completion: nil)
        }
    }
    
    let alasanBatal = ["Cuaca Buruk","Kapal Tidak Siap", "Kapal Tunda Tidak Siap", "Dermaga Tidak Siap", "Pandu Tidak Siap"]

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.pickerViewBatal?.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return alasanBatal.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return alasanBatal[row]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
