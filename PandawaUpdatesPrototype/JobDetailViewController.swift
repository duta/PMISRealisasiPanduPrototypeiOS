//
//  JobDetailViewController.swift
//  PandawaUpdatesPrototype
//
//  Created by iLoops on 3/1/16.
//  Copyright © 2016 ptdmc. All rights reserved.
//

import UIKit

class JobDetailViewController: UIViewController {
    
    @IBOutlet weak var updateButton: UIButton?
    
    @IBAction func updateButtonPressed(sender:AnyObject){
        print("update button pressed")
        if (updateButton?.currentTitle == "PANDU SIAP"){
            updateButton?.setTitle("NAIK KAPAL", forState: UIControlState.Normal)
        }
        else if (updateButton?.currentTitle == "NAIK KAPAL"){
            updateButton?.setTitle("KAPAL GERAK", forState: UIControlState.Normal)
        }
        else if (updateButton?.currentTitle == "KAPAL GERAK"){
            updateButton?.setTitle("SELESAI", forState: UIControlState.Normal)
        }
        else if (updateButton?.currentTitle == "SELESAI"){
            updateButton?.setTitle("TURUN", forState: UIControlState.Normal)
        }
        else if (updateButton?.currentTitle == "TURUN"){
            if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("realisasifinish") as? UIViewController {
                presentViewController(resultController, animated: true, completion: nil)
            }
        }

        
        
    }
    
    @IBAction func batalButtonPressed(sender:AnyObject){
        print("button batal pressed")
        if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("bataljob") as? UIViewController {
            presentViewController(resultController, animated: true, completion: nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
