//
//  View2ControllerViewController.swift
//  PandawaUpdatesPrototype
//
//  Created by iLoops on 2/28/16.
//  Copyright © 2016 ptdmc. All rights reserved.
//

import UIKit

class View2ControllerViewController: UIViewController, UITextFieldDelegate  {

    @IBAction func buttonMasukLoginPressed(sender:AnyObject){
        print("button Login pressed")
        if let resultController = self.storyboard!.instantiateViewControllerWithIdentifier("joblist") as? UIViewController {
            presentViewController(resultController, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
